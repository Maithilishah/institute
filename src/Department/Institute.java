/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Department;

/**
 *
 * @author Admin
 */
public class Institute {
    
    private String name;
    private int groupofdepartment;
    
    public Institute(String name, int groupofdepartment) {
        this.name = name;
        this.groupofdepartment = groupofdepartment;
    }
    
    public String getName() {
        return this.name;
    }
    
     public int getgroupofdepartment() {
        return this.groupofdepartment;
    }
}
