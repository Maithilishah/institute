/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Department;

/**
 *
 * @author Admin
 */
public class Department {
    
    private String name;
    private int groupofstudents;
    
    public Department(String name, int groupofstudents) {
        this.name = name;
        this.groupofstudents = groupofstudents;
    }
    
    public String getName() {
        return this.name;
    }
    
     public int getgroupofstudents() {
        return this.groupofstudents;
    }
}
